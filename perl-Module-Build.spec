%{?perl_default_filter}
%global __requires_exclude %{?__requires_exclude:%__requires_exclude|}^perl\\((ExtUtils::Install|File::Spec|Module::Build|Module::Metadata|Perl::OSType)\\)$
%global __requires_exclude %__requires_exclude|^perl\\(CPAN::Meta::YAML\\) >= 0.002$

Name:           perl-Module-Build
Epoch:          2
Version:        0.42.34
Release:        2
Summary:        Build and install Perl modules
License:        GPL-1.0-or-later OR Artistic-1.0-Perl
URL:            https://metacpan.org/release/Module-Build
Source0:        https://cpan.metacpan.org/authors/id/L/LE/LEONT/Module-Build-0.4234.tar.gz
Patch0:         Module-Build-0.4231-Do-not-die-on-missing-ExtUtils-CBuilder-in-have_c_co.patch
BuildArch:      noarch

BuildRequires:  coreutils, perl-interpreter, perl-devel, perl-generators, perl(Archive::Tar), perl(AutoSplit), perl(base), perl(Carp), perl(Config)
BuildRequires:  perl(CPAN::Meta) >= 2.142060, perl(CPAN::Meta::Converter) >= 2.141170, perl(CPAN::Meta::Merge), perl(CPAN::Meta::YAML) >= 0.003
BuildRequires:  perl(Cwd), perl(Data::Dumper), perl(DynaLoader), perl(Exporter), perl(ExtUtils::CBuilder) >= 0.27, perl(ExtUtils::Install) >= 0.3
BuildRequires:  perl(ExtUtils::Installed), perl(ExtUtils::Manifest) >= 1.54, perl(ExtUtils::Mkbootstrap), perl(ExtUtils::Packlist), perl(ExtUtils::ParseXS) >= 2.21
BuildRequires:  perl(File::Basename), perl(File::Compare), perl(File::Copy), perl(File::Find), perl(File::Path), perl(File::Spec) >= 0.82, perl(File::Spec::Functions)
BuildRequires:  perl(File::Temp) >= 0.15, perl(Getopt::Long), perl(inc::latest), perl(lib), perl(Module::Metadata) >= 1.000002, perl(Parse::CPAN::Meta) >= 1.4401
BuildRequires:  perl(Perl::OSType) >= 1, perl(strict), perl(Archive::Zip), perl(File::ShareDir) >= 1.00, perl(TAP::Harness), perl(TAP::Harness::Env)
BuildRequires:  perl(Test::Harness) >= 3.29, perl(Test::More) >= 0.49, perl(Text::ParseWords), perl(utf8), perl(vars), perl(version) >= 0.87, perl(warnings)
Requires:       perl(CPAN::Meta) >= 2.142060, perl(CPAN::Meta::Converter) >= 2.141170
Requires:       perl(CPAN::Meta::Merge), perl(ExtUtils::Install) >= 0.3, perl(ExtUtils::Manifest) >= 1.54, perl(ExtUtils::Mkbootstrap), perl(ExtUtils::ParseXS) >= 2.21
Requires:       perl(inc::latest), perl(Module::Metadata) >= 1.000002, perl(Perl::OSType) >= 1, perl(TAP::Harness::Env), perl(Test::Harness), perl(Software::License), perl(blib)
Requires:       perl(Pod::Html), perl(Pod::Man) >= 2.17, perl(Pod::Text)
Recommends:     perl(ExtUtils::CBuilder) >= 0.27

%description
Module::Build is a system for building, testing, and installing Perl modules.
It is meant to be an alternative to ExtUtils::MakeMaker. Developers may alter
the behavior of the module through subclassing. It also does not require a make
on your system - most of the Module::Build code is pure-perl and written in a
very cross-platform way.

%package        help
Summary:        man files for %{name}

%description    help
This package includes man files for %{name}.

%prep
%autosetup -n Module-Build-0.4234 -p1

%build
perl Build.PL --installdirs=vendor
./Build

%install
./Build install --destdir=$RPM_BUILD_ROOT --create_packlist=0
%{_fixperms} -c $RPM_BUILD_ROOT/

%check
rm -f t/signature.t
LANG=C TEST_SIGNATURE=1 MB_TEST_EXPERIMENTAL=1 ./Build test

%files
%license LICENSE
%doc contrib/ README Changes
%{_bindir}/config_data
%{perl_vendorlib}/*

%files help
%{_mandir}/man*/*

%changelog
* Sat Jan 18 2025 Funda Wang <fundawang@yeah.net> - 2:0.42.34-2
- drop useless perl(:MODULE_COMPAT) requirement

* Thu Dec 28 2023 liyanan <liyanan61@h-partners.com> - 2:0.42.34-1
- upgrade version to 0.42.34

* Tue Feb 02 2021 shixuantong <shixuantong@huawei.com> - 2:0.42.31-1
- upgrade version to 0.42.31

* Fri Nov 29 2019 openEuler Buildteam <buildteam@openeuler.org> - 2:0.42.24-12
- Package init
